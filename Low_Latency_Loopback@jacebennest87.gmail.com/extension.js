
const St = imports.gi.St;
const Main = imports.ui.main;
const GLib = imports.gi.GLib;

let state, button, icon, onIcon;

function _toggle() {

    if (state == 0) {
        button.set_child(onIcon);
        GLib.spawn_command_line_async('pactl load-module module-loopback latency_msec=1');
        state = 1;
    }
    else {
        button.set_child(icon);
        GLib.spawn_command_line_async('pactl unload-module module-loopback');
        state = 0;
    }
}

function init() {
    button = new St.Bin({ style_class: 'panel-button', reactive: true, can_focus: true,track_hover: true });
    icon = new St.Icon({ style_class: 'pulseaudio-loopback-device-icon' });
    onIcon = new St.Icon({ style_class: 'pulseaudio-loopback-device-on-icon' });

    button.set_child(icon);
    button.connect('button-press-event', _toggle);
    state = 0;
}

function enable() {
    Main.panel._rightBox.insert_child_at_index(button, 0);
}

function disable() {
    Main.panel._rightBox.remove_child(button);
}
